# snowfall

this is a small javascript lib to add a lovely little snow to your Webapplication.
Feel free to use and modify this Code in your own project.

# Howto

to use this script copy the files `snow.js` and `snow.css` in your project and add the following lines to your HTML head:
```
    <!-- Snowflakes from swFox -->
    <link rel="stylesheet" type="text/css" href="snow.css" />
    <script src="snow.js"></script>
```

# Example

to view an example of this snowfall-lib, open https://swfox.gitlab.io/snowfall/ or check this repo out and open the index.html file in your browser.


-sw fox-
