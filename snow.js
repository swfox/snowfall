window.onload = addSnow;

function addSnow () {
	var snowflakes = document.createElement("div");
	snowflakes.className = "snowflakes 2";
	for (i = 0; i < 100; i++) { 
		var snow = document.createElement("div");
		snow.className = "snowflake";
		var v1 = Math.random() * 18 - 18;
		var v2 = Math.random() * 4 - 4;
		var size = 0.8 + Math.random() * 2.4;
		var d1 = 15 + 6 * (2-size) + Math.random() * 3;
		var d2 = 3 + Math.random() * (3-size);
		var blur = (size>2.3 && size<3)?0:((Math.random()>0.5)?0.2:2);
		snow.style.cssText = 'left:'+Math.random()*100+'%;font-size:'+size+'em;animation-duration:'+d1+'s,'+d2+'s;animation-delay:'+v1+'s,'+v2+'s;filter: blur('+blur+'px);';

		var snowflake = document.createTextNode((Math.random()>0.5)?'❅':'❆'); 
		snow.appendChild(snowflake);

		snowflakes.appendChild(snow);
	}
		var body = document.getElementsByTagName('body')[0];
		body.appendChild(snowflakes);
}
